import resolve from '@rollup/plugin-node-resolve';
//import typescript from '@rollup/plugin-typescript';
import ts from "@wessberg/rollup-plugin-ts";

export default {
	input: 'src/index.ts',
	output: [
		{
			format: 'esm',
			file: 'lib/bundle.esm.js'
		},
		{
			format: 'cjs',
			file: 'lib/bundle.cjs.js'
		},
	],
	plugins: [
		resolve(),
		ts()
	]
};