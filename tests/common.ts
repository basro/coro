export { strict as assert } from 'assert';
import * as coro from '../src';

export {coro};

export function run(gen: Generator, maxSteps: number) {
    let steps = 0;
    while( steps < maxSteps ) {
        const {done, value} = gen.next();
        if ( done ) return {steps, done, value};
        steps++;
    }
    return {steps, done:false, value:undefined};
}

export function makeCoroutines(...steps: number[]) {
    const coroutines: coro.Coroutine<number>[] = [];
    const done = [];

    for( let i = 0; i < steps.length; ++i ) {
        const cLength = steps[i];
        const c = function*() {
            try {
                let i = 0;
                for(; i < cLength; ++i ) {
                    yield;
                }
                return i;
            }finally {
                done[i] = true;
            }
        }
        done.push(false);
        coroutines.push(c());
    }

    return {coroutines, done}
}