import { assert, run, makeCoroutines, coro } from '../common';

describe("run", function() {
    it("Runs until all tasks end", function() {
        const {coroutines, done} = makeCoroutines(1,2,4,0);
        const c = coro.run(...coroutines);
        assert.equal(run(c, 100).steps, 4);
        assert(done.every(x => x));
    });

    it("Stops the coroutines if returned externally", function() {
        const {coroutines, done} = makeCoroutines(10, 1, 2, 4);
        const c = coro.run(...coroutines);
        c.next();
        c.next();
        c.next();
        assert.deepEqual(done, [false, true, true, false]);
        c.return();
        assert(done.every(x => x));
    });
});