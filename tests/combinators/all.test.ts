import { assert, run, makeCoroutines, coro } from '../common';

describe("all", function() {
	it("Runs until all tasks end", function() {
		const {coroutines, done} = makeCoroutines(1,2,4,0);
		const c = coro.all(...coroutines);
		assert.equal(run(c, 100).steps, 4);
		assert(done.every(x => x));
	});

	it("Stops the coroutines if returned externally", function() {
		const {coroutines, done} = makeCoroutines(10, 1, 2, 4);
		const c = coro.all(...coroutines);
		c.next();
		c.next();
		c.next();
		assert.deepEqual(done, [false, true, true, false]);
		c.return(undefined!);
		assert(done.every(x => x));
	});

	it("Returns the values of the coroutines", function() {
		const {coroutines, done} = makeCoroutines(1,2,4,0);
		const c = coro.all(...coroutines);
		assert.deepEqual(run(c, 100).value, [1,2,4,0]);
	});
});