import { assert, run, makeCoroutines, coro } from '../common';

describe("race", function() {
    it("Returns the value of the first coroutine to end", function() {
        const {coroutines, done} = makeCoroutines(5,3,10);
        const c = coro.race(...coroutines);
        assert.equal(run(c, 100).value, 3);
    });

    it("Runs until any task ends", function() {
        const {coroutines, done} = makeCoroutines(5,2,3,10);
        const c = coro.race(...coroutines);
        assert.equal(run(c, 100).steps, 2);
    });

    it("Stops the other coroutines when any of them ends", function() {
        const {coroutines, done} = makeCoroutines(10, 3, 2, 4);
        const c = coro.race(...coroutines);
        c.next();
        c.next();
        assert(done.every( x => !x));
        c.next();
        assert(done.every(x => x));
    });

    it("Stops the coroutines if returned externally", function() {
        const {coroutines, done} = makeCoroutines(10, 5, 20, 4);
        const c = coro.race(...coroutines);
        c.next();
        c.next();
        assert(done.every( x => !x));
        c.return(undefined!);
        assert(done.every(x => x));
    });
});