import { assert, run, makeCoroutines, coro } from '../common';

describe("control", function() {
	it("Runs until master task ends", function() {
		const {coroutines: [master, ...slaves], done} = makeCoroutines(3, 5, 1);
		const c = coro.control(master, ...slaves);
        assert.equal(run(c, 10).steps, 3);
        assert(done.every(x=>x), "All coroutines have stopped");
	});

	it("Stops the coroutines if returned externally", function() {
		const {coroutines:[m, ...slaves], done} = makeCoroutines(10, 1, 2, 4);
		const c = coro.control(m, ...slaves);
		c.next();
		c.next();
		c.next();
		assert.deepEqual(done, [false, true, true, false]);
		c.return(undefined!);
		assert(done.every(x => x), "All coroutines have stopped");
	});

	it("Returns the value of the master coroutine", function() {
		const {coroutines: [m, ...slaves], done} = makeCoroutines(5,2,4,10);
		const c = coro.control(m, ...slaves);
		assert.deepEqual(run(c, 100).value, 5);
	});
});