import { assert, run, makeCoroutines, coro } from '../common';

describe("runUntil", function() {
	it("Stops if the predicate is true", function() {
        const {coroutines, done} = makeCoroutines(3, 5, 100);
        let predicate = false;
        const c = coro.runUntil(()=>predicate, ...coroutines);
        assert(!run(c, 10).done, "Not done yet");
        predicate = true;
        assert(c.next().done, "Stopped");
        assert(done.every(x=>x), "All coroutines have stopped");
    });

	it("Stops when all the coroutines have stopped", function() {
		const {coroutines, done} = makeCoroutines(10, 1, 2, 4);
        const c = coro.runUntil(()=>false, ...coroutines);
        assert.equal(run(c, 100).steps, 10);
		assert(done.every(x => x), "All coroutines have stopped");
	});
});