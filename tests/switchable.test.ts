import { assert, coro, makeCoroutines, run } from './common';

describe("switchable", function() {
    it("Continues even after the inner coroutine stops", function() {
        const {coroutines:[inner], done} = makeCoroutines(1);
        const c = coro.switchable(inner);

        c.next();
        assert(!done[0], "Inner is not done yet");
        c.next();
        assert(done[0], "Inner is now done");
        c.next();
        c.next();
        assert(!c.next().done, "Switchable coroutine continued to run");
    });

    it("Switches coroutines and stops the previous one", function() {
        const {coroutines, done} = makeCoroutines(2,10,10);
        const c = coro.switchable();

        c.next();
        c.switch(coroutines[0]);
        run(c, 10);
        assert(done[0], "First coroutine is done");
        c.switch(coroutines[1]);
        run(c,5);
        assert(!done[1], "Second coroutine is not done");
        c.switch(coroutines[2]);
        assert(done[1], "Second coroutine is done");
        run(c,5);
        assert(!done[2], "Third coroutine is not done");
        c.switch(null);
        assert(done[2], "Third coroutine is done");
        run(c,10);
    });

    it("Works with reentry", function() {
        let c: coro.SwitchableCoroutine;
        let {coroutines:[c2], done} = makeCoroutines(2);

        let testDone = false;

        function* test() {
            try {
                yield;
                c.switch(c2);
                yield;
                assert.fail("This should never happen");
            } finally {
                testDone = true;
            }
        }

        c = coro.switchable(test());

        c.next();
        assert(!testDone, "Coroutine not done yet");
        assert(!done[0], "Coroutine not done yet");
        c.next();
        assert(testDone, "First coroutine is done");
        assert(!done[0], "Second coroutine is not done yet");
        c.next();
        c.next();
        assert(!done[0], "Second coroutine is not done yet");
        c.next();
        assert(done[0], "Second coroutine is done");
    });
});