import { assert, run, makeCoroutines, coro } from './common';

describe("stoppable", function() {
    it("Returns the value that the inner coroutine returns", function(){
        const {coroutines, done} = makeCoroutines(10);
        const c = coro.stoppable(coroutines[0]);
        assert.equal(run(c, 100).value, 10);
    });

    it("Stops when the inner coroutine stops", function() {
        const {coroutines, done} = makeCoroutines(10);
        const c = coro.stoppable(coroutines[0]);
        assert.equal(run(c, 100).steps, 10);
    });

    it("Stops the inner coroutine immediately when stop method is called", function(){
        const {coroutines:[inner], done} = makeCoroutines(10);
        const c = coro.stoppable(inner);

        c.next();
        c.next();
        assert.equal(done[0], false);
        c.stop();
        assert.equal(done[0], true);
    });

    it("Stops the inner coroutine when stop is called reentrantly", function() {
        let c: coro.StoppableCoroutine;
        let done = false;
        function* test(){
            try {
                yield;
                c.stop();
                yield;
            } finally {
                done = true;
            }
        }
        c = coro.stoppable(test());
        c.next();
        assert(!done, "Not done yet");
        c.next();
        assert(done, "Is done");
    });
});
