import { Coroutine } from '../coroutine';

export function* waitForPromise<T>(promise: Promise<T>): Coroutine<T> {
    let done = false;
    let error = false;
    let result: any;
    promise.then(
        v => { result = v; done = true; },
        e => { result = e; error = true; }
    );
    while(true) {
        if ( error ) throw result;
        if ( done ) return result;
        yield;
    }
}