import { Coroutine } from '../coroutine';
import { StoppableCoroutine, stoppable } from './stoppable';

export interface SwitchableCoroutine extends Coroutine {
    switch(coroutine: Coroutine | null | undefined): void;
}

class SwitchCoroutine implements SwitchableCoroutine {
    private _child: StoppableCoroutine<unknown> | null | undefined = null;
    private c: Coroutine;

    constructor(coroutine?: Coroutine) {
        this.c = this.main();
        this.switch(coroutine);
    }

    switch(coroutine: Coroutine | null | undefined) {
        this._child?.stop();
        this._child = coroutine && stoppable(coroutine);
    }

    next() {
        return this.c.next();
    }

    return() {
        return this.c.return(undefined);
    }

    throw(e: any) {
        return this.c.throw(e);
    }

    [Symbol.iterator]() {
        return this.c;
    }

    private *main() {
        try {
            while(true) {
                this._child?.next();
                yield;
            }
        }finally {
            this._child?.return(undefined);
        }
    }
}

export function switchable(coroutine?: Coroutine<unknown>): SwitchableCoroutine {
    return new SwitchCoroutine(coroutine);
}