import { Coroutine } from '../coroutine';

export interface StoppableCoroutine<T = unknown> extends Coroutine<T | undefined> {
    stop(): void;
}

class Stoppable<T> implements StoppableCoroutine<T> {
    private c: Coroutine<T | undefined>;
    private shouldStop = false;
    private reentrant = false;

    constructor(coroutine: Coroutine<T>) {
        this.c = this.generator(coroutine);
    }

    next() {
        return this.c.next();
    }

    return(value: T) {
        return this.c.return(value);
    }

    throw(e: any) {
        return this.c.throw(e);
    }

    [Symbol.iterator]() {
        return this.c;
    }

    stop() {
        if ( !this.reentrant ) {
            this.reentrant = true;
            this.c.return(undefined);
        }
        this.shouldStop = true;
    }

    private *generator(inner: Coroutine<T>): Coroutine<T | undefined> {
        try {
            while(true) {
                this.reentrant = true;
                const {done, value} = inner.next();
                this.reentrant = false;
                if ( this.shouldStop ) return undefined;
                if ( done ) return value as T;
                yield;
            }
        }finally {
            this.reentrant = true;
            inner.return(undefined!);
        }
    }
}

export function stoppable<T>(coroutine: Coroutine<T>): StoppableCoroutine<T> {
    return new Stoppable(coroutine);
}