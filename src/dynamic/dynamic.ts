import { Coroutine } from '../coroutine';

export interface DynamicCoroutine extends Coroutine<void> {
    add(child: Coroutine): void;
}

class Dynamic implements DynamicCoroutine {
    private c: Coroutine<void>;
    private children: Coroutine[] = [];

    constructor() {
        this.c = this.generator();
    }

    next() {
        return this.c.next();
    }

    return() {
        return this.c.return();
    }

    throw(e: any) {
        return this.c.throw(e);
    }

    [Symbol.iterator]() {
        return this.c;
    }

    add(child: Coroutine) {
        this.children.push(child);
    }

    private *generator(): Coroutine<void> {
        try {
            while(true) {
                const children = this.children;
                let index = 0;
                for ( let i = 0; i < children.length; ++i ) {
                    const child = children[i];
                    if ( !child.next().done ) {
                        children[index++] = child;
                    }
                }
                children.length = index;
                yield;
            }
        }finally {
            for ( let child of this.children ) {
                child.return(undefined);
            }
        }
    }
}

export function dynamic(): DynamicCoroutine {
    return new Dynamic();
}