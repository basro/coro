/**
 * A coroutine is a generator that yields void and may return a type.
 */
export type Coroutine<T = unknown> = Generator<void, T, void>;
