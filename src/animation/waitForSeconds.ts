import { DeltaTime } from './deltaTime';
import { Coroutine } from '../coroutine';

/**
 * A coroutine that ends after a certain amount of seconds.
 * 
 * Depends on the value of the DeltaTime reference being updated externally between
 * each yield to reflect the amount of time passed since the last time it was resumed.
 * 
 * @param dt a reference to a DeltaTime object that should be updated externally.
 * @param seconds the number of seconds to wait for.
 */
export function* waitForSeconds(dt: DeltaTime, seconds: number): Coroutine<void> {
    let time = seconds;
    while ( time > 0 ) {
        time -= dt.value;
        yield;
    }
}