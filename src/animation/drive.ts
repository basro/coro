import { DeltaTime } from './deltaTime';
import { Coroutine } from '../coroutine';


export function drive( routine:(dt:DeltaTime)=>Coroutine ) {
    let dt = {value: 0.0};
    let time = performance.now();
    const c = routine(dt);
    function handleAnimationFrame() {
        const now = performance.now();
        dt.value = (now - time) / 1000;
        time = now;
        let {done} = c.next();
        if (!done) {
            requestAnimationFrame(handleAnimationFrame);
        }
    }
    requestAnimationFrame(handleAnimationFrame);
}