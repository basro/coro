/**
 * A utility type to pass delta type by reference instead of by value.
 */
export interface DeltaTime {
    value: number;
}