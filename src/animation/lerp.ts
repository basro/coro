import { DeltaTime } from './deltaTime';

/**
 * A coroutine that interpolates a value over a specified amount of time.
 * 
 * Depends on the value of the DeltaTime reference being updated externally between
 * each yield to reflect the amount of time passed since the last time it was resumed.
 * 
 * @param dt a reference to a DeltaTime object that should be updated externally.
 * @param duration the number of seconds it takes to interpolate from start to end.
 * @param start the starting value.
 * @param end the target end value.
 * @param func a callback function that is called on every continuation of the with an updated value.
 */
export function* lerp(dt: DeltaTime, duration: number, start:number, end: number, func: (value: number) => void) {
    const increment = (end - start) / duration;
    let time = 0;
    while ( time < duration ) {
        time += dt.value;
        if ( time > duration ) time = duration;
        const value = start + increment * time;
        func(value);
        yield;
    }
}