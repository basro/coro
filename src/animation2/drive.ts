import { stepWithDeltaTime } from './deltaTime';
import { Coroutine } from '../coroutine';

export function drive( c:Coroutine ) {
    let time = performance.now();
    function handleAnimationFrame() {
        const now = performance.now();
        const dt = (now - time) / 1000;
        time = now;
        let {done} = stepWithDeltaTime(dt, c);
        if (!done) {
            requestAnimationFrame(handleAnimationFrame);
        }
    }
    requestAnimationFrame(handleAnimationFrame);
}