import { Coroutine } from '../coroutine';
import { deltaTime, stepWithDeltaTime } from './deltaTime';

export function* maxTimestep<T>(maxStepSize: number, coroutine:Coroutine<T>): Coroutine<T> {
    try {
        while(true) {
            let dt = deltaTime;
            while( dt > 0 ) {
                const step = Math.min(dt, maxStepSize);
                dt -= step;
                const {done, value} = stepWithDeltaTime(step, coroutine);
                if (done) return value as T;
            }
            yield;
        }
    }finally {
        coroutine.return(undefined!);
    }
}