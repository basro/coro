import { Coroutine } from '../coroutine';
import { deltaTime, stepWithDeltaTime } from './deltaTime';

export function* fixedTimestep<T>(stepSize: number, coroutine:Coroutine<T>): Coroutine<T> {
    let time = 0;
    try {
        while(true) {
            time += deltaTime;
            while( time > 0 ) {
                time -= stepSize;
                const {done, value} = stepWithDeltaTime(stepSize, coroutine);
                if ( done ) return value as T;
            }
            yield;
        }
    }finally {
        coroutine.return(undefined!);
    }
}