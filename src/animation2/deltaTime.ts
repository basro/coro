import { Coroutine } from '../coroutine';

export let deltaTime = 1;

export function stepWithDeltaTime<T>(dt:number, coroutine: Coroutine<T>): IteratorResult<void, T> {
    const old = deltaTime;
    deltaTime = dt;
    try {
        return coroutine.next();
    } finally {
        deltaTime = old;
    }
}

export function* runWithDeltaTime<T>(dtFunc: () => number, coroutine: Coroutine<T>): Coroutine<T> {
    try {
        while(true) {
            const {done, value} = stepWithDeltaTime(dtFunc(), coroutine);
            if ( done ) {
                return value as T;
            }
            yield;
        }
    }finally {
        coroutine.return(undefined!);
    }
}