import { Coroutine } from '../coroutine';

export function* all<T>(...coroutines: Coroutine<T>[]): Coroutine<T[]> {
    const result: (T | undefined)[] = [];
    result.length = coroutines.length;
    try {
        while( true ) {
            let end = result.length;
            let resultDone = true;
            for ( let i = 0; i < end; i++ ) {
                if ( result[i] === undefined ) {
                    const {done, value} = coroutines[i].next();
                    if ( done ) {
                        result[i] = value as T;
                    }else {
                        resultDone = false;
                    }
                }
            }
            if ( resultDone ) return result as T[];
            yield;
        }
    }finally {
        for ( let coroutine of coroutines ) {
            coroutine.return(undefined!);
        }
    }
}
