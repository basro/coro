import { Coroutine } from '../coroutine';

/**
 * A coroutine that will run other coroutines in parallel and ends when all of them have ended.
 * 
 * @param coroutines a list of coroutines to run in parallel.
 */
export function *run(...coroutines: Coroutine<unknown>[]) {
    let activeCoroutines = coroutines.length;
    try {
        while ( true ) {
            let allDone = true;
            let index = 0;
            for ( let i = 0; i < activeCoroutines; ++i ) {
                const coroutine = coroutines[i];
                if ( !coroutine.next().done ) {
                    coroutines[index++] = coroutine;
                }
            }
            if ( index == 0 ) return;
            activeCoroutines = index;
            yield;
        }
    }finally {
        for ( let i = 0; i < activeCoroutines; ++i ) {
            coroutines[i].return(undefined);
        }
    }
}
