import { Coroutine } from '../coroutine';

/** 
 * A coroutine that runs master and slaves in parallel until the master coroutine ends.
 * 
 * @param master the controlling coroutine.
 * @param slaves the coroutines that run in parallel to master.
 */
export function *control<T>(master: Coroutine<T>, ...slaves: Coroutine[]): Coroutine<T> {
    try {
        while ( true ) {
            const {done, value} = master.next();
            if ( done ) return value as T;
            for ( let slave of slaves ) {
                slave.next();
            }
            yield;
        }
    }finally {
        master.return(undefined!);
        for ( let slave of slaves ) {
            slave.return(undefined!);
        }
    }
}
