import { Coroutine } from '../coroutine';

/**
 * A coroutine that will run other coroutines in parallel and ends when any of them ends.
 * 
 * @param coroutines a list of coroutines to to run in parallel.
 */
export function *race<T>(...coroutines: Coroutine<T>[]): Coroutine<T> {
    try {
        while ( true ) {
            for ( let coroutine of coroutines ) {
                const {done, value} = coroutine.next();
                if ( done ) {
                    return value as T;
                }
            }
            yield;
        }
    } finally {
        for ( let coroutine of coroutines ) {
            coroutine.return(undefined!);
        }
    }
}
