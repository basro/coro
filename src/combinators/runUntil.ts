import { Coroutine } from '../coroutine';

/**
 * A coroutine that will run coroutines in parallel until a predicate is true.
 * 
 * @param predicate a callback funtion that stops the coroutine when true.
 */
export function *runUntil(predicate: () => boolean, ...coroutines: Coroutine[]): Coroutine<void> {
    try {
        while( !predicate() ) {
            let done = true;
            for( let coroutine of coroutines ) {
                if ( !coroutine.next().done ) done = false;
            }
            if ( done ) return;
            yield;
        }
    }finally {
        for( let coroutine of coroutines ) {
            coroutine.return(undefined!);
        }
    }
}
