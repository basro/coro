/**
 * A coroutine that ends when any keyboard key is pressed.
 */
export function* waitForAnyKey() {
    let done = false;
    let key = "";
    const listener = (e?:KeyboardEvent) => {
        done = true;
        key = e?.key || "";
        document.removeEventListener("keydown", listener);
    }
    document.addEventListener("keydown", listener);
    try {
        while( !done ) yield;
        return key;
    }finally {
        listener();
    }
}