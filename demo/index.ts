import * as coro from '../src';

const div = document.querySelector(".message-container")!;
const dyn = coro.dynamic();

function *logMessage(message: string) {
    const msgDiv = document.createElement("div");
    msgDiv.className = "message";
    msgDiv.textContent = "_"
    msgDiv.style.opacity = "0";
    div.appendChild(msgDiv);

    msgDiv.scrollIntoView({block:"center", behavior: "smooth"});
    yield* coro.anim2.lerp(0.4, 0, 1, v => msgDiv.style.opacity = ""+v);
    msgDiv.style.opacity = null!;

    yield* coro.anim2.waitForSeconds(0.5);
    yield* coro.anim2.fixedTimestep(0.02, function*() {
        for( let i = 0; i <= message.length; i++ ) {
            msgDiv.textContent = message.substr(0, i) + "_";
            yield;
        }
    }());

    msgDiv.textContent = message;
    yield* coro.anim2.waitForSeconds(0.5);

    dyn.add(function*() {
        yield* coro.anim2.waitForSeconds(1);
        yield* coro.anim2.lerp(1, 1, 0, v => msgDiv.style.backgroundColor = `rgba(238,238,238,${v})`);;
    }());
}

function* main() {
    for(let i = 0; i < 10; i++) {
        yield* logMessage("This is a demo of coro!");
        yield* logMessage("Coro is a collection of tools for working with generator based coroutines.");
        yield* logMessage("It's pretty useful for animations.");
        yield* logMessage("But it's got many other uses too.");
        yield* logMessage("It's very composable!");
        yield* logMessage("You can run things in parallel:");
        yield* coro.run(logMessage("* Like this for example."), logMessage("* And this too."), logMessage("* And this."));
        yield* logMessage("You can wait for user input:");
        yield* logMessage("PRESS ANY KEY");
        const key = yield* coro.input.waitForAnyKey();
        yield* logMessage(`You pressed "${key}"`);
    }
}

coro.anim2.drive(coro.run(dyn, main()));