import resolve from '@rollup/plugin-node-resolve';
import ts from "@wessberg/rollup-plugin-ts";
import copy from 'rollup-plugin-copy';

export default {
	input: 'index.ts',
	output: {
		format: 'esm',
		file: '../public/demo.js'
	},
	plugins: [
		resolve(),
		ts(),
		copy({
			targets:[
				{src:"index.html", dest:"../public/"}
			]
		})
	]
};